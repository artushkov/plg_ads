-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Фев 05 2016 г., 14:17
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `opendb`
--

-- --------------------------------------------------------

--
-- Структура таблицы `ads`
--

DROP TABLE IF EXISTS `ads`;
CREATE TABLE IF NOT EXISTS `ads` (
  `id_ad` int(11) NOT NULL AUTO_INCREMENT,
  `visible` int(2) NOT NULL DEFAULT '0',
  `customer_name` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `price` text NOT NULL,
  `ad_text` text NOT NULL,
  `ad_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_ad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=65 ;

--
-- Дамп данных таблицы `ads`
--

INSERT INTO `ads` (`id_ad`, `visible`, `customer_name`, `phone`, `email`, `price`, `ad_text`, `ad_date`) VALUES
(39, 0, 'Вихляев Виталий', '', 'VVATORgmail.com', '0', 'Продаю коляску Б2', '2015-12-28 14:20:04'),
(40, 1, 'Артюшков Владислав', '', 'vladislavgmail.com', '0', 'Куплю Б23', '2015-12-28 14:22:55'),
(41, 0, '123', '', '123', '0', '123', '2015-12-28 15:40:17'),
(42, 0, 'цук', '', '234', '0', '234234', '2015-12-28 15:42:37'),
(43, 0, 'цук', '', '999999999', '0', '234234', '2015-12-28 15:42:53'),
(44, 0, '11111', '', '33333', '0', '444444', '2015-12-28 15:43:42'),
(45, 0, '11111', '', '33333', '0', '444444', '2015-12-28 15:44:59'),
(46, 1, '11111', '', '33333', '0', '33333333333', '2015-12-28 15:45:28'),
(47, 1, 'Виталий', '38-61', 'VVATORgmail.com', '0', 'Программист', '2015-12-28 15:45:40'),
(54, 1, 'Vladislav', '+375296545361', '33333', '0', '444444', '2015-12-31 09:07:17'),
(56, 0, '', '', '', '0', '', '2016-01-20 10:44:11'),
(59, 1, 'Vlad', '111111', 'pochta', '5', 'opt', '2016-01-21 08:42:07'),
(60, 0, '', '', '', '', '', '2016-02-01 14:08:37'),
(61, 0, '', '', '', '', '', '2016-02-01 14:09:19'),
(62, 0, '', '', '', '', '', '2016-02-01 14:34:18'),
(63, 0, '1', '2', '3', '4', '5', '2016-02-01 14:34:31'),
(64, 0, '', '', '', '', '', '2016-02-02 05:15:53');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
