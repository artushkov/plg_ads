<?php
// HTTP
define('HTTP_SERVER', 'http://opencart/admin/');
define('HTTP_CATALOG', 'http://opencart/');

// HTTPS
define('HTTPS_SERVER', 'http://opencart/admin/');
define('HTTPS_CATALOG', 'http://opencart/');

// DIR
define('DIR_APPLICATION', 'Z:\home\opencart\www/admin/');
define('DIR_SYSTEM', 'Z:\home\opencart\www/system/');
define('DIR_DATABASE', 'Z:\home\opencart\www/system/database/');
define('DIR_LANGUAGE', 'Z:\home\opencart\www/admin/language/');
define('DIR_TEMPLATE', 'Z:\home\opencart\www/admin/view/template/');
define('DIR_CONFIG', 'Z:\home\opencart\www/system/config/');
define('DIR_IMAGE', 'Z:\home\opencart\www/image/');
define('DIR_CACHE', 'Z:\home\opencart\www/system/cache/');
define('DIR_DOWNLOAD', 'Z:\home\opencart\www/download/');
define('DIR_LOGS', 'Z:\home\opencart\www/system/logs/');
define('DIR_CATALOG', 'Z:\home\opencart\www/catalog/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'opendb');
define('DB_PREFIX', '');
?>