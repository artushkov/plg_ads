-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Фев 11 2016 г., 18:48
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `opendb`
--

-- --------------------------------------------------------

--
-- Структура таблицы `ads`
--

CREATE TABLE IF NOT EXISTS `ads` (
  `id_ad` int(11) NOT NULL AUTO_INCREMENT,
  `visible` int(2) NOT NULL DEFAULT '0',
  `customer_name` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `price` text NOT NULL,
  `ad_text` text NOT NULL,
  `ad_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_ad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=77 ;

--
-- Дамп данных таблицы `ads`
--

INSERT INTO `ads` (`id_ad`, `visible`, `customer_name`, `phone`, `email`, `price`, `ad_text`, `ad_date`) VALUES
(40, 0, 'Артюшков Владислав', '', 'vladislavgmail.com', '0', 'Куплю Б23', '2015-12-28 14:22:55'),
(42, 0, 'цук', '', '234', '0', '234234', '2015-12-28 15:42:37'),
(54, 1, 'Vladislav', '+375296545361', '33333', '0', '444444', '2015-12-31 09:07:17'),
(65, 1, '1', '23', '4343543', '545435', '453', '2016-02-05 13:45:59'),
(76, 0, 'sdasda', 'sdasda', 'asdaas', 'asasasas', 'dsds', '2016-02-09 14:33:49');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
