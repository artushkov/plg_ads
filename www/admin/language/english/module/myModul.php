<?php
// Heading
$_['heading_title']       = 'myModul';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Settings module is updeted';
$_['text_content_top']    = 'Up page';
$_['text_content_bottom'] = 'Bottom page';
$_['text_column_left']    = 'Left column';
$_['text_column_right']   = 'Right column';

// Entry
$_['entry_layout']        = 'Chem:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort:';

// Error
$_['error_permission']    = 'Permission denied for control this module!';
?>