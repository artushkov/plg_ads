<?php
// HTTP
define('HTTP_SERVER', 'http://plg_ads/admin/');
define('HTTP_CATALOG', 'http://plg_ads/');

// HTTPS
define('HTTPS_SERVER', 'http://plg_ads/admin/');
define('HTTPS_CATALOG', 'http://plg_ads/');

// DIR
define('DIR_APPLICATION', 'Z:\home\plg_ads\www/admin/');
define('DIR_SYSTEM', 'Z:\home\plg_ads\www/system/');
define('DIR_DATABASE', 'Z:\home\plg_ads\www/system/database/');
define('DIR_LANGUAGE', 'Z:\home\plg_ads\www/admin/language/');
define('DIR_TEMPLATE', 'Z:\home\plg_ads\www/admin/view/template/');
define('DIR_CONFIG', 'Z:\home\plg_ads\www/system/config/');
define('DIR_IMAGE', 'Z:\home\plg_ads\www/image/');
define('DIR_CACHE', 'Z:\home\plg_ads\www/system/cache/');
define('DIR_DOWNLOAD', 'Z:\home\plg_ads\www/download/');
define('DIR_LOGS', 'Z:\home\plg_ads\www/system/logs/');
define('DIR_CATALOG', 'Z:\home\plg_ads\www/catalog/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'opendb');
define('DB_PREFIX', '');
?>