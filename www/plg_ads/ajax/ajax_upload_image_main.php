﻿ <html>
<head>


<!-- Latest compiled and minified CSS & JS -->
	<link rel="stylesheet" media="screen" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
	<script src="//code.jquery.com/jquery.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>	






<title>Ajax Image Upload Using PHP and jQuery</title>
<meta charset="UTF-8">

<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed|Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
	var index_count=1;
  var status_img = new Array(true,true,true,true,true);

</script>
<script src="/plg_ads/ajax/script.js"></script>
<link rel="stylesheet" href="/plg_ads/ajax/style.css" />
</head>
<body>

<div class="container-fluid">
    <div id="_ads_add"  class="row">

      <div id="_ads_add_data" class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding:0px;">
      <form action="<?=$ads_full_path?>/index1.php" method="get" accept-charset="utf-8">
      <div class="ads_row row">
          <div class="col-md-6" >Ваше имя:*<br>
      <input type="text" name="customer_name" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" />
      </div>
      <div class="col-md-6">Регион:*<br>
            <select  name="customer_city" id="customer_city" class="form-control" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" required="required">
                  <option value="Гомель">Гомель</option>
                  <option value="Минск">Минск</option>
                  <option value="Брест">Брест</option>
                  <option value="Гродно">Гродно</option>
                  <option value="Витебск">Витебск</option>
                  <option value="Могилев">Могилев</option>
            </select>
      </div>
      </div>


      <div class="ads_row row">
        <div class="col-md-6">
          Телефон:*
          <br>     
          <input type="text" name="phone" class="col-xs-12 col-sm-12 col-md-12 col-lg-12"  />      
        </div>

        <div class="col-md-6">
          E-mail:
          <br>      
          <input  type="text" name="email" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" />      
        </div>
      </div>


      <div class="ads_row row" >
        <div id="ads_block_textarea" class="col-md-12" >
          Объявление:*
          <br>      
          <textarea placeholder="Текст вашего объявления не должен привышать 500 символов" class="form-control vresize col-xs-12 col-sm-12 col-md-12 col-lg-12" name="ad_text" id="ads_field_text" cols="37" rows="10"></textarea>
        </div>
      </div>
      <div class="ads_row row">
        <div class="col-md-4">
          Цена:*
          <br>      
          <input type="text" name="price" />      
          <br></div>
        <div class="col-md-4">
          Введите код с картинки:*
          <br>      
          <img style="border: 1px solid gray; background: url('<?=$ads_full_path?>/bg_capcha.png');"
                  src=" <?=$ads_full_path?>/captcha.php" width="120" height="40"/>
          <br>      
          <input type="text" name="captcha"/>      
        </div>
       	<div class="col-md-4">
       		 <input type="submit" id="ads_button_submit" value="Отправить" onClick="copy_value_img(); this.submit();" class=" btn btn-large btn-block btn-success" >
           <input id="url_img_1" type="hidden" name="url_img_1" value="">
           <input id="url_img_2" type="hidden" name="url_img_2" value="">
           <input id="url_img_3" type="hidden" name="url_img_3" value="">
           <input id="url_img_4" type="hidden" name="url_img_4" value="">
           	 <? if (isset($_SESSION['sys_message'])){
             	echo $_SESSION['sys_message'];
             	}?>
      </div>
      </div>
      </form>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 im">
        <div id="_ads_block_img" class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="padding:0px;"><!--  col-img -->
        	<div id="ads_add_img" class="main">
        		<form id="uploadimage" action="" method="post" enctype="multipart/form-data" ><!-- id="img_upload" -->
              <img id="previewing" src="/plg_ads/ajax/no_img.jpg" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" />
        		<hr id="line">
        		<div id="selectImage">
        		<label>Добавить изображение</label><br/>
        		<input type="file" name="file" id="file" value="Upload" required />
        <!--  <input type="submit" value="Upload" onclick="upload_img()" class=" btn btn-large btn-block "/>  -->
            <input id="param_index_count" type="hidden" name="param_index_count" value="">
        		</div>
        		</form>
        		<h4 id='loading'></h4>
        		<div id="message" style="visibility: hidden;" ></div>
        	</div>
        </div>
        <div id=""  class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="padding:0px;">      
          	<div  id="block1">
            <input name="url_img1"  type="hidden" id="url_img1"  >
               <img   src="/plg_ads/ajax/no_img.jpg" alt="" id="min_img1"   class="col-xs-12 col-sm-12 col-md-12 col-lg-12"  >
            </div>
          	<div  id="block2">
            <input name="url_img2" type="hidden" id="url_img2"  >            
               <img  src="/plg_ads/ajax/no_img.jpg" alt="" id="min_img2"  class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
            </div>
          	<div  id="block3">
            <input name="url_img3" type="hidden" id="url_img3"  >            
               <img  src="/plg_ads/ajax/no_img.jpg" alt="" id="min_img3"  class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
            </div>
          	<div  id="block4">
            <input name="url_img4" type="hidden" id="url_img4"  >            
               <img  src="/plg_ads/ajax/no_img.jpg" alt="" id="min_img4"  class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
            </div>
        </div>
      </div>
    </div>
</div>




<script src="/javascripts/application.js" type="text/javascript" charset="utf-8" async defer>

</script>

</body>
</html>