<?header('Content-Type: text/html; charset=utf-8');
include "db.php";
include "constants.php";

dbConection($dbHost, $dbName, $dbUser, $dbPassword);

include "pagination_controller.php";
?>

<!doctype html>
<html lang="en">
<head>
<link rel="stylesheet" href="styles/CSS.css">
	<meta charset="UTF-8">
	<title>Document</title>
	<!-- Latest compiled and minified CSS & JS -->
	<link rel="stylesheet" media="screen" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
	<script src="//code.jquery.com/jquery.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>

	<style >
		.aj_success_table_row{
			border:5px solid green !important;
		}
		.aj_error_table_row{
			border:5px solid red !important;
		}
		.aj_complete_table_row{
			border:5px solid blue !important;
		}
	</style>	

	<script src="/plg_ads/js/grid_admin.js" type="text/javascript" charset="utf-8" async defer></script>



	<script type="text/javascript">
	var edit=false;
	var st=false;
	$(document).ready(function(){
		
		$("input").change(function(e){
			edit=true;
		//	alert(edit);
	 	});	

		$("#ads_batch_visible").change(function(e){
			st=true;	
		//	alert("st"+st);
	 	});	

	 	$("#ads_batch_update").click(function(e){
	 		var operation="";
	 		
	 		if (st===true){
	 		operation+="STATUS";
	 		}
	 		
			if ((st===true)&&(edit===true)){
				operation+=";";
			}
	 		if (edit===true){
	 		operation+="EDIT";
	 		}
	 		if ((st===true)&&(edit===true)){
				operation+=";";
			}
			$("#ads_batch_update").val(operation);
	 		alert("edit="+edit+ "status="+st+"op"+operation);
	 	});











		$("#btn_aj").click(function(e){
			var msg_info = $('.panel.panel-info .panel-body');
			msg_info.text('');
			var arr = $('.ads_grid_check:checked');
			$('.ads_grid_check:checked').each(function () { 
					var obj = $(this);
					var id= obj.val();
					
					var table_row =(obj.parent().parent());	

							var ads_id=table_row.find('#ads_id_'+id);
							var set_visible=table_row.find('#set_visible_'+id);
							var customer_name=table_row.find('#customer_name_'+id);
							var phone=table_row.find('#phone_'+id);
							var email=table_row.find('#email_'+id);
							var price=table_row.find('#price_'+id);
							var ad_text=table_row.find('#ad_text_'+id);
							var ad_data=table_row.find('#ad_data_'+id);
							var min_img1=table_row.find('#min_img1_'+id);
							var min_img2=table_row.find('#min_img2_'+id);
							var min_img3=table_row.find('#min_img3_'+id);
							var min_img4=table_row.find('#min_img4_'+id);
							var customer_city=table_row.find('#customer_city_'+id);
							

							$.ajax({  	
							type: "GET",
						    url: '/plg_ads/aj_batch_edit_controller.php',         
						    dataType: "html",
						    data:   '&ads_id='+id+
									'&set_visible='+set_visible.val()+
									'&customer_name='+customer_name.val()+
									'&phone='+phone.val()+
									'&email='+email.val()+
									'&price='+price.val()+
									'&ad_text='+ad_text.val()+
									'&ad_data='+ad_data.val()+
									'&min_img1='+min_img1.attr('src')+
									'&min_img2='+min_img2.attr('src')+
									'&min_img3='+min_img3.attr('src')+
									'&min_img4='+min_img4.attr('src')+
									'&customer_city='+customer_city.val(),
						    success: function(data) { 
						    	 	 table_row.addClass('aj_complete_table_row');
									 msg_info.append(data);
						    },	
						      error: function() { 
						      	alert("error");	
						      	table_row.addClass('aj_error_table_row');	
						      	msg_info.append("НЕ УДАЛОСЬ ОТРЕДАКТИРОВАТЬ ЗАПИСЬ: "+id+"<br>");				
						    },
						      complete: function() {
						
								
								 setTimeout(function(){  table_row.removeClass('aj_complete_table_row').removeClass('aj_error_table_row');}, 10000);

							
						    },		               
							});

			});
				
	 	});	

////////////////////////////////////////////// DELETE  DELETE  DELETE  DELETE  DELETE  DELETE  DELETE 

$("#ads_batch_delete").click(function(e){
			var msg_info = $('.panel.panel-info .panel-body');
			msg_info.text('');
			var arr = $('.ads_grid_check:checked');
			$('.ads_grid_check:checked').each(function () { 
					var obj = $(this);
					var id= obj.val();
					alert(id.value);	
					var table_row =(obj.parent().parent());	

							var ads_id=table_row.find('#ads_id_'+id);
							var set_visible=table_row.find('#set_visible_'+id);
							var customer_name=table_row.find('#customer_name_'+id);
							var phone=table_row.find('#phone_'+id);
							var email=table_row.find('#email_'+id);
							var price=table_row.find('#price_'+id);
							var ad_text=table_row.find('#ad_text_'+id);
							var ad_data=table_row.find('#ad_data_'+id);
							var min_img1=table_row.find('#min_img1_'+id);
							var min_img2=table_row.find('#min_img2_'+id);
							var min_img3=table_row.find('#min_img3_'+id);
							var min_img4=table_row.find('#min_img4_'+id);
							var customer_city=table_row.find('#customer_city_'+id);
							

							$.ajax({  	
							type: "GET",
						    url: '/plg_ads/aj_batch_delete_controller.php',         
						    dataType: "html",
						    data:   '&ads_id='+id,
									
						    success: function(data) { 
						    	 	 table_row.remove();
								$('.panel.panel-info .panel-body').append(data);

						    },	
						      error: function() { 
						      	alert("error");	
						      	table_row.addClass('aj_error_table_row');
						        msg_info.append("НЕ УДАЛОСЬ УДАЛИТЬ ЗАПИСЬ: "+id+"<br>");					
						    },
	               
							});
			});		
	 	});	


//////////////////////////////////////////////








   });

	</script>


	


</head>
<body>




<style>
#insert_table{
border:1px solid #cdcdcd;
}
tr{
border:1px solid #cdcdcd;
}
td{
border:1px solid #cdcdcd;
}
</style>

<div id="panel-info" class="panel panel-info">
	  <div class="panel-heading">
			<h3 class="panel-title">Статус <button type="button" id="btn_visble_info" name="btn_visble_info" class="btn btn-danger pull-right"><span class="glyphicon glyphicon-remove"></span> </button></h3>
			
	  </div>
	  <div class="panel-body">
		<?=$_SESSION['sys_message']?>
	  </div>
</div>

<form action="/plg_ads/admin_table_controller.php" method="post" accept-charset="utf-8">
	
<div class="panel panel-default">
	<div class="panel-body">
	 <button type="button" id="ads_batch_delete" name="ads_batch_delete" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Удалить</button>
	 <button type="button" id="btn_aj" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span> Изменить</button>
			
	</div>

<div class="panel-body">
	 	<select id="ads_batch_visible" name="ads_batch_visible" class="ads_tabl_size">
				<option value="1"
						<?if($visible=="1"){
					//	echo "selected='selected'";
						}?>
				>Да</option>
				<option value="0" 
						<?if($visible=="0"){
					//	echo "selected='selected'";
						}?>
				>Нет</option>
			</select>	
			<button type="submit"  id="ads_batch_update" name="ads_batch_update">Сохранить</button>
</div>


</div>

	
<table id = "insert_table" class="table table-hover" >

	<thead> 

		<tr>
			<td>
				<input type="checkbox" id="ads_grid_check_all" name="ads_grid_check_all">
			
				<!-- <input  type="text" name="selected_check[]" class="selected_check"> -->
		
			</td>
			<td id="ads_id">Id</td>
			<td id="ads_status">Статус</td>
			<td id="ads_name">Имя</td>
			<td id="ads_tel">Телефон</td>
			<td id="ads_email">Почта</td>
			<td id="ads_price">Цена товара</td>
			<td id="ads_text">Описание</td>
			<td id="ads_data">Дата</td>
			<td id="ads_img">Картинки</td>
			<td id="customer_city">Город</td>
		</tr>
	</thead>

		<tbody>

	<?
	dbConection($dbHost, $dbName, $dbUser, $dbPassword);
	if(!isset($ads_data)){
	$ads_data=select_ads_pagination($PAGINATION_DEFAULT_LIMIT,$PAGINATION_DEFAULT_OFFSET);
}
	$number=0;
	while ($row = mysql_fetch_array($ads_data, MYSQL_NUM)) { 
	   // printf ("id_ad: %-10s|  customer_name: %-10s|<br>", $row[0], $row[1]);  
		$number++;
		$id_ad= $row[0];
        $visible=$row[1];
        $customer_name=$row[2];
        $phone=$row[3];
        $email=$row[4];
        $price=$row[5];
        $ad_text=$row[6];
        $ad_date=$row[7];
        $img_1=$row[8];
        $img_2=$row[9];
        $img_3=$row[10];
        $img_4=$row[11];
        $customer_city=$row[12];
	?>
	
		<tr>
			<td><input type="checkbox" id="ads_grid_check_<?=$number?>" name="selected_check[]" value="<?=$id_ad?>" number="<?=$number?>" class="ads_grid_check" ></td>
			<td><?=$id_ad?><input type="hidden" name="id_ad"  value="<?=$id_ad?>" /></td> 
			<td>
			<select id="set_visible_<?=$id_ad?>" name="set_visible[]" class="ads_tabl_size">
				<option value="1"
						<?if($visible=="1"){
						echo "selected='selected'";
						}?>
				>Да</option>
				<option value="0" 
						<?if($visible=="0"){
						echo "selected='selected'";
						}?>
				>Нет</option>
			</select>
			</td>










			<td><input type="text" id='customer_name_<?=$id_ad?>' class="ads_tabl_size" name="customer_name[]" value="<?=$customer_name?>"/></td>
			<td><input type="text" id='phone_<?=$id_ad?>' class="ads_tabl_size" name="phone[]" value="<?=$phone?>"/></td>
			<td><input type="text" id='email_<?=$id_ad?>' class="ads_tabl_size" name="email[]" value="<?=$email?>"/></td>
			<td><input type="text" id='price_<?=$id_ad?>' class="ads_tabl_size" name="price[]" value="<?=$price?>"/></td>
			<td><textarea name="ad_text[]"  id='ad_text_<?=$id_ad?>' class="ads_tabl_size"><?=$ad_text?></textarea></td>
			<td><?=$ad_date?></td>
			<td> <div id="ads_ribon_img" class="row">
			     <div id="block1"  class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding:0px;">
			         
			             <input name="url_img1"  type="hidden" id="url_img1"  > 
			             <img   src="<?="/".$ads_full_path."/ajax/".$img_1?>" alt="" id="min_img1_<?=$id_ad?>"  onClick="set_previewing('#previewing<?=$id_ad?>','<?="/".$ads_full_path."/ajax/".$img_1?>')" class="col-xs-12 col-sm-12 col-md-12 col-lg-12"  >
			             
			     </div>
			     <div id="block2"  class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding:0px;">
			         
			             <input name="url_img2" type="hidden" id="url_img2"  > 
			             <img  src="<?="/".$ads_full_path."/ajax/".$img_2?>" alt="" id="min_img2_<?=$id_ad?>" onClick="set_previewing('#previewing<?=$id_ad?>','<?="/".$ads_full_path."/ajax/".$img_2?>')" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
			            
			     </div>
			     <div id="block3"  class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding:0px;">
			        
			             <input name="url_img3" type="hidden" id="url_img3"  > 
			             <img  src="<?="/".$ads_full_path."/ajax/".$img_3?>" alt="" id="min_img3_<?=$id_ad?>" onClick="set_previewing('#previewing<?=$id_ad?>','<?="/".$ads_full_path."/ajax/".$img_3?>')" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
			           
			     </div>
			     <div id="block4"  class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding:0px;">
			      
			             <input name="url_img4" type="hidden" id="url_img4"  > 
			             <img  src="<?="/".$ads_full_path."/ajax/".$img_4?>" alt="" id="min_img4_<?=$id_ad?>"  onClick="set_previewing('#previewing<?=$id_ad?>','<?="/".$ads_full_path."/ajax/".$img_4?>')" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
			       
			     </div>
			     
			 </div>
			 </td>
			<td>
			     <select  class="ads_tabl_size" id="customer_city_<?=$id_ad?>" name="customer_city[]" value="<?=$customer_city?>">
                  <option  value="<?=$customer_city?>" selected><?=$customer_city?></option>
                  <option value="Гомель">Гомель</option>
                  <option value="Минск">Минск</option>
                  <option value="Брест">Брест</option>
                  <option value="Гродно">Гродно</option>
                  <option value="Витебск">Витебск</option>
                  <option value="Могилев">Могилев</option>
            </select>
            </td>
			<td><button type="submit"  class="btn btn-warning" name="fixed_ad" value="edit"><span class=" glyphicon glyphicon-pencil"/></button></td>
			<td>


			<button type="submit" onclick="$('#number_row').val('<?=$id_ad?>');" class="btn btn-danger" name="delete_id_ad" value="Delete"><span class="glyphicon glyphicon-trash"/>	</button> 	</td>

		</tr>	
	<?
	}
	?>
	</tbody>
</table>
<input type="text" id="number_row" name="number_row" value="Delete"><?=$id_ad?>



<ul class="pagination">
	<li><a href="#">&laquo;</a></li>
	<li><a href="#">1</a></li>
	<li><a href="#">2</a></li>
	<li><a href="#">3</a></li>
	<li><a href="#">4</a></li>
	<li><a href="#">5</a></li>
	<li><a href="#">&raquo;</a></li>
</ul>
</form>

<? include("pagination_view.php");


echo $_SERVER['HTTP_REFERER'];
echo "last_page ".$last_page;
echo "page_count ".$page_count;
echo "record_block_count ".$record_block_count;
echo "page".$page;

?>






<style>
.ads_tabl_size	{
	width: 100%;
}

#ads_id {

}

#ads_status{
	width: 70px;
}
#ads_name{}
#ads_tel{}
#ads_email{}
#ads_price{
	width: 100px;
}
#ads_text	{}		
#ads_data{}


</style>

</body>
</html>